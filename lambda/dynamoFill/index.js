const AWS = require('aws-sdk');
const data = require('./data.json');

const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10', region: 'ap-southeast-2'});

module.exports.handler = async (event) => {

    for (let i = 0; i < data.length; i++){
        let params = {
            Item: {
                "postcodeId" : {
                    N: i.toString()
                },
                "minPostcode"   : {
                    S: data[i].minPostcode
                },
                "maxPostcode" : {
                    S: data[i].maxPostcode
                },
                "state" : {
                    S: data[i].state
                },
                "nearestOffice" : {
                    S: data[i].nearestOffice
                }
            },
            TableName: process.env.AUSPOSTCODES //INSERT DYNAMO TABLE NAME
        }
        await dynamodb.putItem(params, function(err,data){
            if (err) console.log(err, err.stack);
            else     console.log(data);
        }).promise();;
    }
}