'use strict';
const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB({apiVersion: '2012-08-10', region: 'ap-southeast-2'});

module.exports.handler = async event => {
    console.log(`Request: ${JSON.stringify(event)}`)
    let messageValue, responseBodyValue, statusCodeValue, params;

    if (!validateInput(event.body)) {
        statusCodeValue = 400;
        messageValue = "Invalid Input - Use the following JSON body {postcode : value} with valid values between 0000 - 9999.";
    } else {
        const jsonBody = JSON.parse(event.body);
        const postcodeDecimal = parseInt(jsonBody.postcode, 10); 
        const postcodeDigit = Math.floor(postcodeDecimal/1000);

        params = {
            Key: {
                "postcodeId": {
                    N: postcodeDigit.toString()
                }
            },
            TableName: process.env.AUSPOSTCODES
        }
        const getItemResponse = await dynamoDb.getItem(params).promise();

        statusCodeValue = 200;
        messageValue = `For customers in ${getItemResponse.Item.state.S}, Lightspeed's closest office is in ${getItemResponse.Item.nearestOffice.S}.`;
    }
    responseBodyValue = {
        message: messageValue,
        input: event,
    }; 
        
    console.log("RESPONSE: " + JSON.stringify(responseBodyValue));
    return {
        statusCode: statusCodeValue,
        body: JSON.stringify(
            responseBodyValue,
            null,
            2
        ),
    };
}

const validateInput = (body) => {
    try {
        const jsonBody = JSON.parse(body);
        const regExp = /^[0-9]{4}$/;

        if (!jsonBody) {
            return false;
        }

        if (!jsonBody.postcode) {
            return false;
        }

        const rawPostcode = jsonBody.postcode;

        if (!regExp.test(rawPostcode)) {
            return false;
        }
        
        const numPostcode = parseInt(rawPostcode, 10);
        
        if (numPostcode < 0 || numPostcode > 9999) {
            return false;
        }
        
        return true;
    } catch (e) {
        console.error(`Unexpected Error - ${e}`)
        return false;
    }
}