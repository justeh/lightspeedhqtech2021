# lightspeedhqtech2021

## Task
Your challenge: build a web service. It will receive a postcode and reply with a message like the following:

>For customers in {state}, Lightspeed's closest office is in {city}.

Using the following postcode blocks as reference data:
| Block | State | Nearest Office |
| --- | --- | --- |
| `0000-0999` | Northern Territory | Brisbane |
| `1000-1999` | New South Wales | Sydney |
| `2000-2999` | New South Wales | Sydney |
| `3000-3999` | Victoria | Melbourne |
| `4000-4999` | Queensland | Sydney |
| `5000-5999` | South Australia | Melbourne |
| `6000-6999` | Western Australia | Melbourne |
| `7000-7999` | Tasmania | Melbourne |
| `8000-8999` | Victoria | Melbourne |
| `9000-9999` | Queensland | Brisbane |


## Implementation Plan

### Architecture
API GW -> Lambada (NodeJS) -> DynamoDB

### CI/CD Pipeline
GitLab CI using the serverless framework as a means to deploy the infrastructure
- Initially I plan to use a DynamoDB as my choice of database, which upon creating the infrastructure I will use a lambda to insert the provided data.
- If I have time I will want to play around with Aurora Serverless.

How to setup the pipeline
- Create AWS user with least privilege access (temporary assignment of AdministratorAccess)
- Ensure that AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY is enabled in the Gitlab CI/CD pipelines (Settings -> CI/CD -> Variables)
- CI/CD will run automatically after either a git push or execution in the Gitlab project

### Code
I will be creating two lambdas:
1. nearestOffice - which validates the request, retrieves relevant postcode data and responds task string.
2. dynamoFill - used as a means to enter the provided postcode data in the dynamoDb. Invoke lambda for desired environment after creation (potential to automated).

#### API assumptions
Request: Only supporting JSON body and POST, example curl command
>curl https://<APIGW_ENDPOINT>/production/nearestOffice -H "Content-Type: application/json" --data @data.json -X POST
data.json = {
    postcode: "XXXX"
}

### Operations:
- Logging: Request, Response and errors are being logged with Cloudwatch logs.
- Metrics: Throttles, errors, invocations and duration will have alarms set to it, upon triggered will send email to the support email via SNS.

### Endpoints
- Staging: https://fagwrs9hr5.execute-api.ap-southeast-2.amazonaws.com/staging/nearestOffice
- Production: https://vgzhe7tlxe.execute-api.ap-southeast-2.amazonaws.com/production/nearestOffice
